﻿using GA.BasicTypes;
using GA.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GA
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var ga = new GeneticAlgorithm(4, 8, new TwoPointCrossover(), new InversionMutation(), new RouletteWheelSelection(), x => 2*Math.Pow(x, 2) + 1);
                ga.RunSimulation(100);
                Console.ReadKey();
                break;
            }
            Console.Clear();

            var ind1 = new Individual(10);
            var ind2 = new Individual(10);
            Console.WriteLine("-- Before crossing");
            Console.WriteLine("Individual 1: ");
            Console.WriteLine(ind1);
            Console.WriteLine("Individual 2: ");
            Console.WriteLine(ind2);

            var crossover = new TwoPointCrossover();

            crossover.Crossover(ind1, ind2);
            Console.WriteLine("-- After crossing");
            Console.WriteLine("Individual 1: ");
            Console.WriteLine(ind1);
            Console.WriteLine("Individual 2: ");
            Console.WriteLine(ind2);

            var mutation = new InversionMutation();


            Console.WriteLine("--Before mutation: ");
            Console.WriteLine("Individual 1: ");
            Console.WriteLine(ind1);

            mutation.Mutation(ind1, 0.9);

            Console.WriteLine("--After mutation: ");
            Console.WriteLine("Individual 1: ");
            Console.WriteLine(ind1);
            return;
        }
    }
}
