using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GA.BasicTypes
{
    public class Individual
    {
        public ChromosomeType Chromosome { get; set; }
        public double Fitness { get; set; }

        public Individual(int chromosomeSize)
        {
            Chromosome = new ChromosomeType(chromosomeSize);
        }

        public void UpdateFitness(Func<double, double> fitness)
        {
            Fitness = fitness(Chromosome.DecodedValue);
        }

        public void InsertGenes(int insertIndex, bool[] genes)
        {
            Chromosome.Genes = Chromosome.Genes
                .Select((x, i) => i < insertIndex ? x : genes[i - insertIndex])
                .ToArray();
        }
        public void InsertGenes2Points(int insertIndex1,int insertIndex2, bool[] genes)
        {
            Chromosome.Genes = Chromosome.Genes
                .Select((x, i) => i < insertIndex1 || i >= insertIndex2 ? x : genes[i - insertIndex1])
                .ToArray();
        }
        public void MergeAndInsertGenes(int insertIndex, bool[] genes1, bool[] genes2)
        {
            Chromosome.Genes = Chromosome.Genes
                .Select((x, i) => i < genes2.Count() ? genes2[i] : genes1[i - genes2.Count()])
                .ToArray();
        }
        public void ReplaceGenes(bool[] genes)
        {
            InsertGenes(0, genes);
        }

        public Individual Clone()
        {
            return new Individual(Chromosome.Size)
            {
                Chromosome = Chromosome.Clone()
            };
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.Append("(");
            foreach (var gene in Chromosome.Genes)
            {
                sb.Append($"{(gene ? 1 : 0)} ");
            }
            sb.AppendLine(")");

            sb.AppendLine($"Decoded value: {Chromosome.DecodedValue}");

            return sb.ToString();
        }
    }
}
