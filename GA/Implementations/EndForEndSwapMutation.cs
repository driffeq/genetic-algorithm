﻿using GA.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.BasicTypes;

namespace GA.Implementations
{
    class EndForEndSwapMutation : IMutationOperator
    {
        public void Mutation(Individual individual, double mutationProbability)
        {
            var swapPoint = Enumerable.Range(1, individual.Chromosome.Genes.Count() - 1)
                .OrderBy(x => Guid.NewGuid())
                .First();

            Console.WriteLine($"Swap point: {swapPoint}");

            var firstGenomePart = individual.Chromosome.Genes
                .TakeWhile((x, i) => i < swapPoint)
                .ToArray();
            var secondGenomePart = individual.Chromosome.Genes
                .SkipWhile((x, i) => i < swapPoint)
                .ToArray();
            individual.MergeAndInsertGenes(swapPoint, firstGenomePart, secondGenomePart);
        }
    }
}
