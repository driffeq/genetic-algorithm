﻿using GA.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.BasicTypes;

namespace GA.Implementations
{
    class InversionMutation : IMutationOperator
    {
        public void Mutation(Individual individual, double mutationProbability)
        {
            var inversionPoints = Enumerable.Range(1, individual.Chromosome.Genes.Count() - 1)
                .OrderBy(x => Guid.NewGuid())
                .Take(2)
                .OrderBy(x => x)
                .ToArray();

            Console.WriteLine($"Inversion points: {inversionPoints[0]}, {inversionPoints[1]}");


            var genomePart = individual.Chromosome.Genes
                .SkipWhile((x, i) => i < inversionPoints[0])
                .Take(inversionPoints[1] - inversionPoints[0])
                .Reverse()
                .ToArray();
            individual.InsertGenes2Points(inversionPoints[0], inversionPoints[1], genomePart);
        }
    }
}
