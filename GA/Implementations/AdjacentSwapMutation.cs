﻿using GA.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.BasicTypes;

namespace GA.Implementations
{
    class AdjacentSwapMutation : IMutationOperator
    {
        public void Mutation(Individual individual, double mutationProbability)
        {
            var geneToSwap = Enumerable.Range(1, individual.Chromosome.Genes.Count() - 1)
                .OrderBy(x => Guid.NewGuid())
                .First();
            Console.WriteLine($"Number of gene: {geneToSwap}");
            var temporaryGene = individual.Chromosome.Genes[geneToSwap - 1];
            individual.Chromosome.Genes[geneToSwap - 1] = individual.Chromosome.Genes[geneToSwap];
            individual.Chromosome.Genes[geneToSwap] = temporaryGene;
        }
    }
}
