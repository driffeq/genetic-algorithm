﻿using GA.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.BasicTypes;

namespace GA.Implementations
{
    class TwoPointCrossover : ICrossOperator
    {
        public void Crossover(Individual parent1, Individual parent2)
        {
            var crossPoints = Enumerable.Range(1, parent1.Chromosome.Genes.Count() - 1)
                .OrderBy(x => Guid.NewGuid())
                .Take(2)
                .OrderBy(x => x)
                .ToArray();

            Console.WriteLine($"Cross points: {crossPoints[0]}, {crossPoints[1]}");

            var parent1Genome = parent1.Chromosome.Genes
                .SkipWhile((x, i) => i < crossPoints[0])
                .Take(crossPoints[1] - crossPoints[0])
                .ToArray();

            var parent2Genome = parent2.Chromosome.Genes
                .SkipWhile((x, i) => i < crossPoints[0])
                .Take(crossPoints[1] - crossPoints[0])
                .ToArray();

            parent1.InsertGenes2Points(crossPoints[0],crossPoints[1], parent2Genome);
            parent2.InsertGenes2Points(crossPoints[0],crossPoints[1], parent1Genome);

        }
    }
}
