﻿using GA.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GA.BasicTypes;

namespace GA.Implementations
{
    class RandomSwapMutation : IMutationOperator
    {
        public void Mutation(Individual individual, double mutationProbability)
        {
            var genesToSwap = Enumerable.Range(1, individual.Chromosome.Genes.Count())
                .OrderBy(x => Guid.NewGuid())
                .Take(2)
                .ToArray();

            Console.WriteLine($"Number of genes: {genesToSwap[0]}, {genesToSwap[1]}");
            var temporaryGene = individual.Chromosome.Genes[genesToSwap[0] - 1];
            individual.Chromosome.Genes[genesToSwap[0] - 1] = individual.Chromosome.Genes[genesToSwap[1] -1];
            individual.Chromosome.Genes[genesToSwap[1] - 1] = temporaryGene;
        }
    }
}
